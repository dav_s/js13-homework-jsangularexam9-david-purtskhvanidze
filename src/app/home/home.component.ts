import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Cocktail } from '../shared/cocktail.model';
import { CocktailService } from '../shared/cocktail.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit, OnDestroy {
  cocktails: Cocktail[] = [];
  cocktailsChangeSubscription!: Subscription;
  cocktailsFetchingSubscription!: Subscription;
  isFetching = false;
  modalOpen = false;

  constructor(private cocktailService: CocktailService) { }

  ngOnInit(): void {
    this.cocktails = this.cocktailService.getCocktails();
    this.cocktailsChangeSubscription = this.cocktailService.cocktailChange.subscribe((cocktails: Cocktail[]) => {
      this.cocktails = cocktails;
    });
    this.cocktailsFetchingSubscription = this.cocktailService.cocktailFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });
    this.cocktailService.fetchCocktails();

    console.log(this.cocktails);
  }

  openCocktailModal() {
    this.modalOpen = true;
  }

  closeCocktailModal() {
    this.modalOpen = false;
  }

  // removeDish(id: string) {
  //   this.cocktailService.removeDish(id).subscribe(() => {
  //     this.cocktailService.fetchcocktailes();
  //   });
  // }

  ngOnDestroy() {
    this.cocktailsChangeSubscription.unsubscribe();
    this.cocktailsFetchingSubscription.unsubscribe();
  }

}
