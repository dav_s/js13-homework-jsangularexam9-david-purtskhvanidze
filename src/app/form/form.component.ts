import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { CocktailService } from '../shared/cocktail.service';
import { Cocktail } from '../shared/cocktail.model';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  addForm!: FormGroup;

  constructor(
    private cocktailService: CocktailService,
    private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.addForm = new FormGroup({
      'cocktailName': new FormControl(null, Validators.required),
      'cocktailImg': new FormControl(null, Validators.required),
      'cocktailType': new FormControl(null, Validators.required),
      'cocktailDescription': new FormControl(null, [Validators.required]),
      'ingredients': new FormArray([]),
      'cocktailInstructions': new FormControl(null, [Validators.required]),
    });
  }

  onSubmit() {
    const id = Math.random().toString();
    const cocktail = new Cocktail(
      id,
      this.addForm.value.cocktailName,
      this.addForm.value.cocktailImg,
      this.addForm.value.cocktailType,
      this.addForm.value.cocktailDescription,
      this.addForm.value.ingredients,
      this.addForm.value.cocktailInstructions,
    );

    const next = () => {
      this.cocktailService.fetchCocktails();
      void this.router.navigate(['/'], {relativeTo: this.route});
    };

    this.cocktailService.addCocktail(cocktail).subscribe(next);
  }

  fieldHasError(fieldName: string, errorType: string) {
    const field = this.addForm.get(fieldName);
    return field && field.touched && field.errors?.[errorType];
  }


  addIngredient() {
    const ingredients = <FormArray>this.addForm.get('ingredients');
    const ingredientsGroup = new FormGroup({
      ingredientName: new FormControl(null, Validators.required),
      ingredientAmount: new FormControl(null, Validators.required),
      ingredientMeasure: new FormControl(null, Validators.required),
    });
    ingredients.push(ingredientsGroup);
  }

  getIngredientsControls() {
    return (<FormArray>this.addForm.get('ingredients')).controls;
  }

  getFormValid() {
    return this.addForm.valid;
  }
}
