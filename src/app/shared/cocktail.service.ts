import { Injectable } from '@angular/core';
import { Cocktail } from './cocktail.model';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Injectable()
export class CocktailService {
  cocktailChange = new Subject<Cocktail[]>();
  cocktailFetching = new Subject<boolean>();
  cocktailUploading = new Subject<boolean>();
  cocktailRemoving = new Subject<boolean>();

  private cocktails: Cocktail[] = [];

  constructor(private http: HttpClient) {}

  getCocktails() {
    return this.cocktails.slice();
  }

  fetchCocktails() {
    this.cocktailFetching.next(true);
    this.http.get<{ [id: string]: Cocktail}>('https://rest-b290b-default-rtdb.firebaseio.com/cocktails.json')
      .pipe(map(result => {
        if (result === null) {
          return [];
        }

        return Object.keys(result).map(id => {
          const cocktailData = result[id];
          return new Cocktail(
            id,
            cocktailData.name,
            cocktailData.img,
            cocktailData.type,
            cocktailData.description,
            cocktailData.ingredients,
            cocktailData.instructions,
          );
        });
      }))
      .subscribe(cocktails => {
        this.cocktails = cocktails;
        this.cocktailChange.next(this.cocktails.slice());
        this.cocktailFetching.next(false);
      }, () => {
        this.cocktailFetching.next(false);
      });
  }


  addCocktail(cocktail: Cocktail) {
    const body = {
      name: cocktail.name,
      img: cocktail.img,
      type: cocktail.type,
      description: cocktail.description,
      ingredients: cocktail.ingredients,
      instructions: cocktail.instructions,
    };

    this.cocktailUploading.next(true);

    return this.http.post('https://rest-b290b-default-rtdb.firebaseio.com/cocktails.json', body).pipe(
      tap(() => {
        this.cocktailUploading.next(false);
      }, () => {
        this.cocktailUploading.next(false);
      })
    );
  }

}
